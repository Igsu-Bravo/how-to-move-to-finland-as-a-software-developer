# How to move to Finland as a Software Developer?

This guide will show software developers how to move to Finland, covering everything from making the decision to settling down in Finland. 

## Contributions welcome!
Help me improve this guide on [Gitlab](https://gitlab.com/albacore/how-to-move-to-finland-as-a-software-developer)

## Who can move to Finland?
Anyone who gets a job contract from a company in Finland should be able to move over here. I've seen many individuals moving to Finland from US, India, Iran,...

## Why Finland?
As a software developer, and in geek terms, I'd say I love these things about Finland:
- Almost everything works out of the box  
- Universal health care
- Low poverty
- Most official things are well documented, many of them in English as well
- You can easily survive by speaking only English if your job is in tech 
- Metal
- Lakes
- Clean and tidy cities
- The best response to COVID-19 among European countries according to CNN
- Work life balance

## Why not Finland?
- It can get cold and dark. 
- Research shows that people of color are facing more racism in Finland compared to many other European countries. [YLE](https://yle.fi/uutiset/osasto/news/finland_among_most_racist_countries_in_eu_study_says/10531670)
- Finding a job outside tech might be very hard if you don't know Finnish, or you don't have a western sounding name. [YLE](https://yle.fi/uutiset/osasto/news/researcher_if_theres_a_worker_with_a_finnish_name_theyll_probably_be_hired/11026589)

what else? send a PR

## How the process (including paperwork) looks like?

If you are coming from outside the EU, you need some paper work (it's all online and digital, but you know what I mean) to sort out. 

### WARNING
I'm not a lawyer and I won't take any responsibilities for the consequences of following this guide. Do your own research.

### Source of truth
Visit [the Finnish immigration agency's website](https://migri.fi) to see the most updated regulations about your situation. It's available in English.

Let me explain by a simplified example. 

### An example relocation case

I love reading examples in docs. So here's one imaginary relocation case to show you an overview of the whole process:

- Rosa lives in New York. She has been working as a senior frontend developer for few years now. 
- Rosa has heard a lot about the quality of life in Finland and decides to move to Finland. 
- Rosa goes to Linkeding, searches for Frontend developer roles in Helsinki
- Finds a reputable company, checks their Glassdoor page to make sure it's a good one
- Sends the application
- HR interview
- First technical interview over zoom
- A home assignment is done
- Second technical interview, and talking to the hiring manager  
- Gets a full time job offer, the pay should be at least 3000 euros per month for the easiest residence permit process.  
- The company helps her apply online for a "Specialist" residence permit, then she visits a Finnish embassy, the closest to her in US

At this point she has two options:

**A. Move to Finland right away:**
As she doesn't need a visa to visit Finland and stay here for 3 months thanks to her American passport, she is legally able to work in Finland until her specialist residence permit application is processed (WARNING: check this at migri.fi, I'm not a lawyer, I've just seen it work like that for developers who apply for that kind of residence permit.), so she can just pack her stuff and move to Finland and start working until her residence permit is granted. It's a bit risky because if for some reason her residence permit is not granted, she needs to leave Finland! 

**B. Or she waits until her residence permit is ready:**
After a couple of months her residence card is ready and she can move to Finland 

In both cases usually all the travelling and application expenses are paid by the employer. 

- The company provides her with free accommodation for a couple of months, until she finds a place. 

DONE! She is now working and living in Finland. 

## What companies hire from abroad?
Any company who can't hire enough developers from inside Finland might be interested in hiring from abroad. Your chances are high for finding something if you are good at what you do and have some years of experience under your belt. 

I've seen these companies hiring developers from abroad:
- RELEX [Careers page](https://careers.relexsolutions.com/en/)

(Finnish companies! Send a merge request to add your company if you hire by relocation)
